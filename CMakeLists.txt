cmake_minimum_required(VERSION 3.1)
project(RealSense-CalibrationRW)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

include(ExternalProject)
include(FetchContent)

# Import Calibration Library
ExternalProject_Add(
    librscalibrationtool_external
    PREFIX 3rd_party
    URL /usr/share/doc/librscalibrationtool/api/DynamicCalibrationAPI-Linux-2.13.1.0.tar.gz
    URL_HASH SHA1=807b7f34801f8b520ac445a10375a94e816c8fb8
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ""
    INSTALL_COMMAND ""
    BUILD_IN_SOURCE
    UPDATE_DISCONNECTED
)

set(librscalibrationtooldir "${CMAKE_CURRENT_BINARY_DIR}/3rd_party/src/librscalibrationtool_external/2.13.1.0")
set(librscalibrationtool_lib "${librscalibrationtooldir}/lib/libDSDynamicCalibrationAPI.so")
set(librscalibrationtool_include_dir "${librscalibrationtooldir}/Include")

file(MAKE_DIRECTORY ${librscalibrationtool_include_dir})

message(STATUS "${librscalibrationtooldir}")
add_library(librscalibrationtool SHARED IMPORTED)
set_target_properties(librscalibrationtool PROPERTIES
    IMPORTED_LOCATION ${librscalibrationtool_lib}
    INTERFACE_INCLUDE_DIRECTORIES ${librscalibrationtool_include_dir}
    IMPORTED_NO_SONAME TRUE
)

FetchContent_Declare(json URL https://github.com/nlohmann/json/releases/download/v3.11.2/json.tar.xz)
FetchContent_Declare(fmt_ext URL https://github.com/fmtlib/fmt/archive/refs/tags/10.0.0.tar.gz)
FetchContent_Declare(eigen_ext
    URL https://gitlab.com/libeigen/eigen/-/archive/3.4.0/eigen-3.4.0.tar.gz
    UPDATE_COMMAND ""
    UPDATE_DISCONNECTED
)
set(EIGEN_BUILD_DOC OFF)
set(BUILD_TESTING OFF)
set(EIGEN_BUILD_PKGCONFIG OFF)
FetchContent_MakeAvailable(json fmt_ext eigen_ext)

find_package(realsense2 REQUIRED)

add_executable(flash_calibration src/flash.cpp)
target_compile_options(flash_calibration PRIVATE -Wall -Wextra -Wpedantic)
target_link_libraries(flash_calibration PUBLIC realsense2 librscalibrationtool PRIVATE eigen nlohmann_json fmt)
